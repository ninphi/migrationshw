﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MigrationsHW.Migrations
{
    public partial class PopulatePostsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql(@"
                    INSERT INTO Posts
                    VALUES
                    (1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse iaculis, velit vel ullamcorper venenatis, sapien tellus scelerisque erat, non feugiat risus urna nec justo. Morbi at egestas quam. Sed iaculis ultricies venenatis. Nulla finibus sodales efficitur. Sed mollis ut quam ac feugiat. In in mollis ex, eget consequat nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris nunc ligula, ultricies a sollicitudin malesuada, dapibus nec neque. Ut aliquam purus vel nisl gravida maximus. Suspendisse suscipit, ligula ut dictum ultricies, ante magna fringilla purus, quis consequat tortor dolor vel metus. Aliquam tristique orci sit amet maximus consequat. Donec at accumsan dolor. Fusce ut metus sit amet nisi lacinia rutrum sit amet aliquam turpis. In mattis iaculis mauris. Etiam sit amet sodales mi, eget dignissim purus.'),
                    (2, 'In non orci sed neque gravida lobortis eu vel mi. Nullam eu dictum ante. Praesent ac mauris nec dolor eleifend vehicula id non magna. In ut lobortis nisi, eget luctus felis. Proin elementum ante vel rutrum pulvinar. Aliquam ultrices nulla a metus tristique, in rhoncus lacus vehicula. Sed nisi libero, sodales sed eleifend at, pharetra vitae quam. Sed in mollis tortor, non semper justo. Sed interdum varius laoreet. Nullam egestas eleifend lectus sed laoreet. Quisque fringilla est ac molestie venenatis. Morbi in dolor mollis leo tempor mattis at sed quam. Aenean ut magna ex.'),
                    (3, 'Aenean mattis scelerisque lacus sed mattis. Vestibulum lacus libero, tincidunt non lacinia ut, aliquam in nisi. Etiam tincidunt ligula quis vestibulum vehicula. Nulla viverra orci eu vestibulum egestas. In nunc tellus, feugiat id pretium id, blandit vel nisi. Sed vestibulum diam diam, vitae eleifend turpis vehicula eu. Suspendisse potenti. Donec ultrices ante eget nunc sollicitudin, vitae vehicula mauris congue. Ut tristique aliquet dapibus. Pellentesque est dolor, tristique quis molestie id, pretium at tortor. Etiam ac facilisis turpis, ac faucibus mi. Etiam id turpis a dui pretium imperdiet id porta lorem. Maecenas ultricies, nulla eu lobortis finibus, justo felis condimentum odio, varius ultricies ipsum ligula ut risus. Mauris bibendum, velit quis aliquam interdum, quam mauris suscipit tellus, at accumsan nisi dui vel libero. Maecenas eros dui, finibus vitae nisi commodo, aliquam sollicitudin nunc.');
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql(@"
                    DELETE FROM Posts
                    WHERE (Id = 1 AND UserId = 1)
                        OR (Id = 2 AND UserId = 2)
                        OR (Id = 3 AND UserId = 3);
                ");
        }
    }
}
