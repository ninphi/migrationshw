﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MigrationsHW.Migrations
{
    public partial class PopulateUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql(@"
                    INSERT INTO Users
                    VALUES
                    ('sparda', 'Roman', 'Tiablin', '1998-01-01 00:00:00.000', GETDATE()),
                    ('ninphi', 'Mark', 'Melnikov', '2001-01-01 00:00:00.000', GETDATE()),
                    ('gremlin', 'Georgii', 'Tereshchenko', '1992-01-01 00:00:00.000', GETDATE());
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql(@"
                    DELETE FROM Users
                    WHERE Id = 1
                        OR Id = 2
                        OR Id = 3;
                ");
        }
    }
}
